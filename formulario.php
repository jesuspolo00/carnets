<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>
		Carnet
	</title>
	<link href="https://fonts.googleapis.com" rel="preconnect">
	<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
	<link crossorigin="anonymous" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" rel="stylesheet">
	<script crossorigin="anonymous" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js">
	</script>
	<script crossorigin="anonymous" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js">
	</script>
</head>
<body>
	<div class="container">
		<div class="row p-2">
			<div class="col-lg-3"></div>
			<div class="col-lg-6">
				<div class="card shadow">
					<div class="card-body">
						<form method="POST" enctype="multipart/form-data" action="frente.php">
							<div class="row">
								<div class="col-lg-6 form-group">
									<label class="fw-bold">Documento</label>
									<input type="text" class="form-control" name="documento" required value="1">
								</div>
								<div class="col-lg-6 form-group">
									<label class="fw-bold">Nombre Completo</label>
									<input type="text" class="form-control" name="nombre" required>
								</div>
								<div class="col-lg-6 form-group">
									<label class="fw-bold">Curso / Cargo</label>
									<input type="text" class="form-control" name="cargo" required>
								</div>
								<div class="col-lg-12 form-group mt-2">
									<label class="fw-bold">foto</label>
									<input type="file" class="form-control" name="foto" required accept=".jpg, .jpeg,.png">
								</div>
								<div class="col-lg-12 form-group mt-4">
									<button type="submit" class="btn btn-success">
										Generar Frente
									</button>
<!-- 									<button type="submit" class="btn btn-danger">
										Generar Reverso
									</button> -->
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>