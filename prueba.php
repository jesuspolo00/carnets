<?php
require_once 'bardcode/vendor/autoload.php';

$generator = new Picqer\Barcode\BarcodeGeneratorPNG();

$nombre    = strtoupper('Sebastián David Escaf Tatis');
$documento = '10422591861';
$oficio    = strtoupper('sexto b');

$img = '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($documento, $generator::TYPE_CODE_128, 1.5, 30)) . '">';

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Carnet</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

</head>
<body>
	<div class="bordes">
		<div class="contenido">
			<center>
				<img src="logo.jpg" alt="" width="50" style="margin-top: -8%;">
				<img src="logo2.jpg" alt="" width="150" style="margin-top: -2%;">
				<br>
				<img src="persona.jpg" alt="" width="100" style="margin-top: 5%;">
				<br>
				<p><span style="font-weight: bold;">Eduardo Luis Mendoza Carracedo</span>
					<br>
					<span style="font-weight: bold;">C.C. 1001869752</span>
					<br>
					ARL: AXA Colpatria
					<br>
					TIPO DE SANGRE
					<br>
					Grupo:  O
					<br>
					RH: +
				</p>
			</center>
		</div>
	</div>
</body>
</html>
<style>
	*{
		font-family: 'Open Sans', sans-serif;
	}
	.contenido{
		width: 100%;
		margin-top: 10%;
	}
	/*.bordes{
		padding: 5px;
		width: 90%;
		margin-top: 40%;
		border-left:  1px solid #000;
		border-right:  1px solid #000;
		height: 170px;
	}
*/
	p{
		font-size: 0.60em;
	}
</style>
<script>
	window.print();
</script>
