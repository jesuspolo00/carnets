<?php
require_once 'bardcode/vendor/autoload.php';

$generator = new Picqer\Barcode\BarcodeGeneratorPNG();

$nombre    = strtoupper('MARIA GONZALEZ');
$documento = '1002012694';
$oficio    = strtoupper('operativo');

$img = '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($documento, $generator::TYPE_CODE_128, 1.5, 30)) . '">';

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Carnet</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

</head>
<body>
	<div class="bordes">
		<div class="contenido">
			<center>
				<p style="text-transform: uppercase; margin-bottom: 15%;">
					<b><?=$oficio?></b>
					<br>
					<b><?=$nombre?></b>
					<br>
					<!-- Descomentar esta linea si es carnet nuevo y comentar el documento de abajo -->
					<b><?=$documento?></b>
					<!-- Numero de documento sin el 1 si es para reposicion -->
					<!-- 1048071178 -->
				</p>
				<p style="margin-bottom: 15%;">
					El presente carné es personal e intrasferible, tiene como único objetivo identificar a su portador dentro de las instalaciones.
					<br>
					Válido hasta junio del 2022.
				</p>

				<p style="margin-bottom: 1%;">
					Cra. 54 #96 - 43
					<br>
					PBX: (57) 5 357 0994 – 378 4156
					<br>
					www.playandlearn.edu.co
					<br>
					Barranquilla - Colombia
				</p>
				<br>
				<p>
					<?=$img?>
					<br>
					<b><?=$documento?></b>
					<!-- 1048071178 -->
				</p>
			</center>
		</div>
	</div>
</body>
</html>
<style>
	*{
		font-family: 'Open Sans', sans-serif;
	}
	.contenido{
		width: 100%;
		margin-top: -40%;
	}
	.bordes{
		padding: 5px;
		width: 93%;
		margin-top: 40%;
		border-left:  1px solid #000;
		border-right:  1px solid #000;
		height: 170px;
	}

	p{
		font-size: 0.60em;
	}
</style>
<script>
	window.print();
</script>
