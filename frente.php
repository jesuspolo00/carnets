<?php
require_once 'bardcode/vendor/autoload.php';

$generator = new Picqer\Barcode\BarcodeGeneratorPNG();

if (isset($_POST['nombre'])) {

    if (isset($_FILES['foto']['name'])) {

        $nom_arch   = $_FILES['foto']['name'];
        $ext_arch   = pathinfo($nom_arch, PATHINFO_EXTENSION);
        $fecha_arch = date('YmdHis');

        $nombre_archivo = strtolower(md5($_POST['documento'] . '_' . $_POST['nombre'] . $fecha_arch)) . '.' . $ext_arch;

        $carp_destino = 'fotos/';
        $ruta_img     = $carp_destino . $nombre_archivo;

        if (is_uploaded_file($_FILES['foto']['tmp_name'])) {
            move_uploaded_file($_FILES['foto']['tmp_name'], $ruta_img);
        }
    }
}

$nombre    = mb_strtoupper($_POST['nombre']);
$documento = $_POST['documento'];
$oficio    = mb_strtoupper($_POST['cargo']);

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Carnet</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

</head>
<body>
	<div class="bordes">
		<div class="contenido">
			<center>
				<img src="fondo.png" class="fondo" alt="">
				<p class="nombre"><?=$nombre?></p>
				<p class="cargo"><?=$oficio?></p>
				<div class="circular--portrait">
					<img src="fotos/<?=$nombre_archivo?>" alt="">
				</div>
			</center>
		</div>
	</div>
</body>
</html>
<style type="text/css">
	*{
		font-family: 'Open Sans', sans-serif;
	}
	body{
		outline: 0;
		padding: 0;
		margin: 0;
	}
	.fondo{
		width: 100%;
		height: 319px;
	}
	.nombre{
		position: relative;
		margin-top: -52%;
		font-size: 0.70em;
	}
	.cargo{
		position: relative;
		margin-top: 13%;
		font-size: 0.65em;
	}

	.circular--portrait {
		position: relative;
		width: 122px;
		height: 122px;
		overflow: hidden;
		border-radius: 50%;
		margin-top: -99.5%;
		z-index: -1000 !important;
		overflow: hidden;
	}

	.circular--portrait img {
		width: 100%;
		height: auto;
	}
</style>
<script>
	window.print();
	window.addEventListener("afterprint", function(event) {
		window.location.replace('formulario.php');
	});
</script>
